import numpy as np
import matplotlib . pyplot as plt
import pandas as pd
from matplotlib.colors import ListedColormap
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix, ConfusionMatrixDisplay, accuracy_score, recall_score, precision_score



"""
Datoteka pima-indians-diabetes.csv sadrži mjerenja provedena u svrhu
otkrivanja dijabetesa, pri cemu se u devetom stupcu nalazi klasa 0 (nema dijabetes) ili klasa 1 ˇ
(ima dijabetes). Ucitajte dane podatke u obliku numpy polja ˇ data. Dodajte programski kod u
skriptu pomocu kojeg možete odgovoriti na sljede ´ ca pitanja: ´
a) Na temelju velicine numpy polja ˇ data, na koliko osoba su izvršena mjerenja?
b) Postoje li izostale ili duplicirane vrijednosti u stupcima s mjerenjima dobi i indeksa tjelesne
mase (BMI)? Obrišite ih ako postoje. Koliko je sada uzoraka mjerenja preostalo?
c) Prikažite odnos dobi i indeksa tjelesne mase (BMI) osobe pomocu´ scatter dijagrama.
Dodajte naziv dijagrama i nazive osi s pripadajucim mjernim jedinicama. Komentirajte ´
odnos dobi i BMI prikazan dijagramom.
d) Izracunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost indeksa tjelesne ˇ
mase (BMI) u ovom podatkovnom skupu.
e) Ponovite zadatak pod d), ali posebno za osobe kojima je dijagnosticiran dijabetes i za one
kojima nije. Kolikom je broju ljudi dijagonosticiran dijabetes? Komentirajte dobivene
vrijednosti
"""

data_np = np.loadtxt('pima-indians-diabetes.csv', delimiter=",", skiprows=9)


#1.a.
print(f"Broj mjerenja: {len(data_np)}")

#1.b.
data = pd.DataFrame(data_np)

print(f'Broj dupliciranih: {data.duplicated().sum()}')
print(f'Broj izostalih: {data.isnull().sum()} ')
data = data.drop_duplicates()
data = data.dropna(axis=0) #trebalo je i izbacit sve 0 iz BMI
data = data[data[5]!= 0]
#data = data[data[3]!= 0]


print(f"Broj mjerenja: {len(data)}")



#1.c.

data.plot.scatter(x=7,y=5,s=25)
plt.title('Odnos dobi i BMI')
plt.xlabel('Age(years)')
plt.ylabel('BMI(weight in kg/(height in m)^2)')
plt.show()

#1.d.
print(data[5].max())
print(data[5].min())
print(data[5].mean())

#1.e
data_diagnosed = data[data[8] == 1]
data_undiagnoes = data[data[8] == 0]
print(f"Diagnosed {len(data_diagnosed)}")
print(data_diagnosed[5].max())
print(data_diagnosed[5].min())
print(data_diagnosed[5].mean())
print(f"UnDiagnosed {len(data_undiagnoes)}")
print(data_undiagnoes[5].max())
print(data_undiagnoes[5].min())
print(data_undiagnoes[5].mean())

#2.a.

output_variable = [8]

input_variables = [0,1,2,3,4,5,6,7]

X = data[input_variables].to_numpy()
y = data[output_variable].to_numpy()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 100)

logisticRegression = LogisticRegression()
logisticRegression.fit(X_train,y_train)

y_test_p = logisticRegression.predict(X_test)

disp = ConfusionMatrixDisplay(confusion_matrix(y_test,y_test_p))
disp.plot()
plt.title('Matrica zabune')
plt.show()

print(f'Tocnost: {accuracy_score(y_test, y_test_p)}')
print(f'Preciznost: {precision_score(y_test, y_test_p)}')
print(f'Odziv: {recall_score(y_test, y_test_p)}')
